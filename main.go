package main

/*
	Автоматизированная информационная система на железнодорожном
	вокзале содержит сведения об отправлении поездов дальнего следования.
	Для каждого поезда указывается:
		- номер поезда;
		- станция назначения;
		- время отправления.
	Данные в информационной системе организованы в виде двоичного дерева.
	Разработать программу, которая:
		- обеспечивает первоначальный ввод данных в информационную систему
		и формирование двоичного дерева;
		- производит вывод всего дерева;
		- вводит номер поезда и выводит все данные об этом поезде;
		- вводит название станции назначения и выводит данные о всех поездах, следующих до этой станции.
	Программа должна обеспечивать диалог с помощью меню и контроль
	ошибок при вводе.
*/

import (
	"errors"
	"fmt"
	"regexp"
)

// Train Структура поезда
type Train struct {
	Number        int    // Номер поезда
	Destination   string // Станция назначения
	DepartureTime string // Время отправления
	LeftChild     *Train
	RightChild    *Train
}

// NewTrain функция конструктор
func NewTrain(number int, destination string, departureTime string) *Train {
	return &Train{Number: number, Destination: destination, DepartureTime: departureTime}
}

func insertTrain(root *Train, train *Train) {
	if train.Number < root.Number {
		if root.LeftChild == nil {
			root.LeftChild = train
		} else {
			insertTrain(root.LeftChild, train)
		}
	} else if train.Number > root.Number {
		if root.RightChild == nil {
			root.RightChild = train
		} else {
			insertTrain(root.RightChild, train)
		}
	} else {
		err := errors.New(fmt.Sprintf("поезд с номером %d уже существует", train.Number))
		fmt.Println(err.Error())
	}
}

func printTree(root *Train) {
	if root != nil {
		printTree(root.LeftChild)
		fmt.Printf("Train Number: %d, Destination: %s, Departure Time: %s\n", root.Number, root.Destination, root.DepartureTime)
		printTree(root.RightChild)
	}
}

func findTrain(root *Train, number int) *Train {
	if root == nil || root.Number == number {
		return root
	}

	if number < root.Number {
		return findTrain(root.LeftChild, number)
	}

	return findTrain(root.RightChild, number)
}

func findTrainsByDestination(root *Train, destination string) {
	if root != nil {
		findTrainsByDestination(root.LeftChild, destination)
		if root.Destination == destination {
			fmt.Printf("Train Number: %d, Destination: %s, Departure Time: %s\n", root.Number, root.Destination, root.DepartureTime)
		}
		findTrainsByDestination(root.RightChild, destination)
	}
}

func main() {
	// Создание корневого узла
	var root = NewTrain(8, "Москва", "14:59")

	// Заполнение первоначальными данными
	train1 := NewTrain(3, "СПб", "14:29")
	train2 := NewTrain(10, "Екатеринбург", "04:29")
	train3 := NewTrain(1, "Екатеринбург", "13:29")
	train4 := NewTrain(6, "Женева", "14:29")
	insertTrain(root, train1)
	insertTrain(root, train2)
	insertTrain(root, train3)
	insertTrain(root, train4)

	for {
		fmt.Println("1. Создание поезда")
		fmt.Println("2. Вывести все данные о поездах")
		fmt.Println("3. Вывести данные о конкретном поезде")
		fmt.Println("4. Вывести данные о поездах до указанной станции")
		fmt.Println("0. Выйти из программы")
		fmt.Print("Выберите действие: ")

		var choice int
		fmt.Scanln(&choice)

		switch choice {
		case 1:
			var train Train
			var time string
			fmt.Print("Введите номер поезда: ")
			fmt.Scanln(&train.Number)
			fmt.Print("Введите станцию назначения: ")
			fmt.Scanln(&train.Destination)
			fmt.Print("Введите время отправления: ")
			fmt.Scanln(&time)
			matched, _ := regexp.MatchString("^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", time)
			if !matched {
				fmt.Println("Некорректное время.")
				break
			}
			train.DepartureTime = time

			if root == nil {
				root = &train
			} else {
				insertTrain(root, &train)
			}
			fmt.Println("Данные о поезде добавлены.")
		case 2:
			fmt.Println("Данные о всех поездах:")
			printTree(root)
		case 3:
			var number int
			fmt.Print("Введите номер поезда: ")
			fmt.Scanln(&number)

			train := findTrain(root, number)
			if train != nil {
				fmt.Printf("Данные о поезде: Number: %d, Destination: %s, Departure Time: %s\n", train.Number, train.Destination, train.DepartureTime)
			} else {
				fmt.Println("Поезд не найден.")
			}
		case 4:
			var destination string
			fmt.Print("Введите название станции назначения: ")
			fmt.Scanln(&destination)

			fmt.Printf("Данные о поездах до станции %s:\n", destination)
			findTrainsByDestination(root, destination)
		case 0:
			fmt.Println("Программа завершена.")
			return
		default:
			fmt.Println("Неверный выбор. Попробуйте еще раз.")
		}
		fmt.Println()
	}
}
